﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CheckVowels.Models;

namespace CheckVowels.Controllers
{
    [Route("api/CheckVowels")]
    public class CheckvowelsController : ControllerBase
    {
        [HttpPost]
        public ActionResult Vowels(Checkvowels checkvowels)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? name = checkvowels.Name;
                bool result = Isvowel(name);

                return Ok(result ? "contain Vowel's" : "Does not contain vowel's");

            }
            catch (Exception)
            {

                throw;
            }

        }
        static bool Isvowel(string name)
        {
            name = name.ToLower();
            
            for (int i = 0; i < name.Length; i++)
            {
                if (name[i] == 'a' || name[i] == 'e' || name[i] == 'i' || name[i] == 'o' || name[i] == 'u')
                    return true;
            }
            return false;
        }
    }
}
