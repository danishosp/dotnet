﻿using System.ComponentModel.DataAnnotations;

namespace Salary_Calculation_System.Models
{
    public class SalaryCalculation
    {
        [Required]
        public string? Name { get; set; }
        public float Salary { get; set; }
    }
}
