﻿using System.ComponentModel.DataAnnotations;

namespace Salary_Calculation_System.Models
{
    public class CharityCalculator
    {
        [Required]
        public double Gold { get; set; }
        [Required]
        public double Silver { get; set; }
        [Required]
        public double Savings { get; set; }
    }
}
