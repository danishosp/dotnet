﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Salary_Calculation_System.Models;

namespace Salary_Calculation_System.Controllers
{
    [Route("api/calculationSystem")]
    public class CharityCalculatorController : ControllerBase
    {
        [HttpPost, Route("charityCalculator")]
        public ActionResult Calculator(CharityCalculator charityCalculator)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                double inputGold = charityCalculator.Gold;
                double inputSilver  = charityCalculator.Silver;
                double inputSavings = charityCalculator.Savings;

                if (inputGold >= 7.5 && inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: " + ((inputGold * 50000) * 2.5) / 100 + "\n" + "Silver: " + ((inputSilver * 5000 * 2.5) / 100) + "\n" + "Savings: " + ((inputSavings * 2.5) / 100));
                }

                else if(inputGold >= 7.5 && inputSilver >= 52.5 || inputSavings < 27000)
                {
                    return Ok("Gold: " + ((inputGold * 50000) * 2.5) / 100 + "\n" + "Silver: " + ((inputSilver * 5000 * 2.5) / 100) +"\n"+ "savings: Not Eligible");
                }

                else if(inputGold >= 7.5 && inputSavings >= 27000 || inputSilver < 52.5)
                {
                    return Ok("Gold: " + ((inputGold * 50000) * 2.5) / 100 + "\n" + "Silver: Not Eligible" + "\n" + "Savings: " + ((inputSavings * 2.5) / 100));
                }

                else if(inputSilver >= 52.5 && inputSavings >= 27000)
                {
                    return Ok("Gold: Not Eligible" + "\n" + ("Silver: " + ((inputSilver * 5000 * 2.5) / 100) + "\n" + ("Savings: " + (inputSavings * 2.5) / 100)));
                }

                else if(inputGold >= 7.5)
                {
                    return Ok ("Gold: "+ ((inputGold * 50000) * 2.5)/100);
                }

                else if (inputSilver >= 52.5)
                {
                    return Ok("Silver: " + ((inputSilver * 5000) * 2.5) / 100);
                }

                else if (inputSavings >= 27000)
                {
                    return Ok("Savings: " + (inputSavings* 2.5) / 100);
                }

                else
                {
                    return Ok("You are not eligible");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
