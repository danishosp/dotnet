﻿using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Salary_Calculation_System.Models;

namespace Salary_Calculation_System.Controllers
{
    [Route("api/salaryCalculationSystem")]
    public class SalaryCalculationController : ControllerBase
    {
        [HttpPost, Route ("salaryCalculation")]
        public ActionResult Salary(SalaryCalculation salaryCalculation)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                string? name = salaryCalculation.Name;
                float salary = salaryCalculation.Salary;
                float tax = 0;
                float netSalary = 0;

                if (salary > 50000)
                {
                    tax = salary * 10 / 100;
                    netSalary = salary-tax;
                }
                else if(salary > 30000)
                {
                    tax = salaryCalculation.Salary * 5 / 100;
                    netSalary = salary - tax;
                }
                else
                {
                    netSalary = (float)salary;
                }
                return Ok ("Account holder name " + name + "\n" + "Your Gross Salary" + salary + "\n" + "Your tax deduction "+ tax + "\n" + "Your net salary " + netSalary);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
