﻿namespace Occurrence_Number_Array.Models
{
    public class OccurrenceArray
    {
        public int[]? ArrayInput { get; set; }
        public int Target { get; set; }
    }
}
