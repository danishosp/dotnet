﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Occurrence_Number_Array.Models;

namespace Occurrence_Number_Array.Controllers
{
    [Route("api/OccurrenceArray")]
    public class OccurrenceArrayController : ControllerBase
    {
        [HttpPost]
        public ActionResult Array(OccurrenceArray occurrenceArray)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new
                    {
                        Status = false,
                        Message = string.Join(Environment.NewLine, ModelState.Values.SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage))
                    });
                }
                int[]? arrayInput = occurrenceArray.ArrayInput;
                int target = occurrenceArray.Target;
                int counter = 0;
                for (int i = 0; i < arrayInput.Length; i++)
                {
                    if (arrayInput[i] == target)
                    {
                        counter++;
                    }
                }
                return Ok(counter);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
